﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Module Module1
    Private server As TcpListener ' This is for the TCP/IP Protocol
    Private client As TcpClient
    Dim stream As NetworkStream
    Sub Main()
        Dim port As Integer 'this is the port number
        Dim localAddr As IPAddress ' this is the IP address
        Dim negotiateStream As Security.NegotiateStream = Nothing
        Try
            'define the two values for your Server
            port = 1234
            localAddr = IPAddress.Loopback 'loopbak = 127.0.0.1 = myself

            'Create your server
            server = New TcpListener(localAddr, port)

            'make your server available
            server.Start()
            MessageBox.Show("Server Ready!")


            'this is a block method, it will wait for new connection
            'before continuing
            client = server.AcceptTcpClient
            'MessageBox.Show("Connected to " & IPAddress.Parse(CType(client.Client.LocalEndPoint, IPEndPoint).Address.ToString).ToString)
            Dim msg As String = "Hello Client! You are connected from " & IPAddress.Parse(CType(client.Client.LocalEndPoint, IPEndPoint).Address.ToString).ToString
            Dim bytes() As Byte = System.Text.ASCIIEncoding.ASCII.GetBytes(msg)
            stream = client.GetStream
            stream.Write(bytes, 0, bytes.Length)

        Catch ex As Exception

        End Try
    End Sub

    Sub ServerStop()
        Try
            Dim msg As String = vbNewLine & "The server has closed the connection. Good bye!"
            Dim bytes() As Byte = System.Text.ASCIIEncoding.ASCII.GetBytes(msg)
            stream.Write(bytes, 0, bytes.Length)
            Thread.Sleep(5000)
            stream.Dispose()
            client.Close()
            server.Stop()
            MessageBox.Show("Server Stopped!")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Module
